package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator  implements Comparator<Car> {
	   
	   
	@Override
	public int compare(Car o1, Car o2) {
		// comparing car a and b is complicated so we do it in proper order
		//first we check first part which is carbrand, if not the same then we know which "goes first"
		//if its the same we check second part which is carmodel, if not the same then we know which "goes first"
		//if same then third part which is model option, if the same all the way then order does not matter
		if (o1.getCarBrand().compareTo(o2.getCarBrand()) == 0)
		{
			
			if (o1.getCarModel().compareTo(o2.getCarModel()) == 0)
			{
				return o1.getCarModelOption().compareTo(o2.getCarModelOption()); 
			
			}
			else 
			{
				return o1.getCarModel().compareTo(o2.getCarModel());
			}
		}
		else 
		{
			return o1.getCarBrand().compareTo(o2.getCarBrand());
		}
		
		
		
	}
	}
