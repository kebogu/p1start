package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	//important that we have these variables, else we cant use the comparator for example
	private Comparator<E> comp;
	private Node<E> header;
	
	private int currentSize;
	
	
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		
		
		public CircularSortedDoublyLinkedListIterator() {
			//normal iterator stuff
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	
	
	private static class Node<E> {
		//normal node stuff
		private E element;
		private Node<E> next;
		private Node<E> previous;
		
		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		public Node() {
			this.element = null;
			this.next = null;
			this.previous = null;
		}
		
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}
		
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		//this nuke helper method was added to make things easier (for me at least) since some things need to be set to null in order to be eliminated
		public void Nuke() {
			this.element = null;
			this.next = null;
			this.previous = null;
		}
		
		
	}
	
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		//normal constructor stuff (note the header has to be a new node, else you will get nullpointerexceptions (bad stuff)
		//this.comp = comp; allows us to use the comparator
		this.header = new Node<E>();
		this.comp = comp;
		this.currentSize = 0;
		
		this.header.setNext(header);
		this.header.setPrevious(header);
		
	}
	
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		//makes a new iterator
		return new CircularSortedDoublyLinkedListIterator<E>();
		
	}


	
	@Override
	public boolean add(E obj) {
		Node<E> newNode = new Node<E>();
		newNode.Nuke();
		//nuke method in action giving us a usable node
		newNode.setElement(obj);
		this.currentSize++;
		
		Node<E> temp = null;
			for (temp = header.getNext(); temp != header ; temp = temp.getNext()) {
				if(comp.compare(temp.getElement(), obj) >= 0) 
					//comp in action letting us know if the obj goes before the temp.getElement()
				{
					newNode.setNext(temp);
					newNode.setPrevious(temp.getPrevious());
					temp.getPrevious().setNext(newNode);
					temp.setPrevious(newNode);
					return true;
				}
			}
			//in case it goes after
			newNode.setNext(header);
			newNode.setPrevious(header.getPrevious());
			header.getPrevious().setNext(newNode);
			header.setPrevious(newNode);
			return true;
			
		}
	

	@Override
	public int size() {
		// self explanatory
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		// we check if its empty, if its not we use a while loop to go through all the list till it finds the obj, once it does it gets "nuked" (removed)
		//if we reach the end, the loop breaks, and returns false since its not in the list
		Node<E> temp = header.getNext();
		if(this.isEmpty())
		{
			return false;
		}
		while(!temp.equals(header)) 
		{
			if(comp.compare( temp.getElement(),  obj) == 0)
			{
				temp.getPrevious().setNext(temp.getNext());
				temp.getNext().setPrevious(temp.getPrevious());
				temp.Nuke();
				this.currentSize--;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
		
	
	}

	@Override
	public boolean remove(int index) {
		// same logic as previous remove but here we pay attention to the position given by index
		//as such we make considerations for invalid cases
		if (this.size() < index || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		if(this.isEmpty())
		{
			return false;
		}
		//then just do as we did previously
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < this.currentSize; i++) {
			if(i == index)
			{
				temp.getPrevious().setNext(temp.getNext());
				temp.getNext().setPrevious(temp.getPrevious());
				temp.Nuke();
				this.currentSize--;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
		
	}

	@Override
	public int removeAll(E obj) {
		// with the if and while statements we check if obj is present throughout the list, and we count how many times we have to remove it using counter
		int counter = 0;
		if(this.isEmpty())
		{
			return counter;
		}
		while(this.contains(obj))
		{
			this.remove(obj);
			counter++;
		}
		return counter;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
//first is the thing that comes after the header
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		// since the whole thing is conceptualized as a circle, or a loop, then the last thing is what comes before the header, hence header.getPrevious().getElement();
		return header.getPrevious().getElement();
	}

	@Override
	public E get(int index) {
	
		if(this.isEmpty())
		{
			return null;
		}
		//we iterate throughout the list till we get a match with the index, once we do we return the element there, else we return null
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < this.currentSize; i++) {
			if(i == index)
			{
				return temp.getElement();
			}
			temp = temp.getNext();
		}
		return null;
		
	}

	@Override
	public void clear() {
		// if its not empty, remove the first thing, repeat this until it is empty
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		// TODO Auto-generated method stub
		//if it finds it even once, it means that yes, it is contained in the list
		return this.firstIndex(e) >= 0;
	}

	//looks good
	@Override
	public boolean isEmpty() {
		// self explanatory
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		if(this.isEmpty()) {
			return -1;
		}
		
		//if its not empty, we go through the list checking each element, if we find it, we return the index of the object
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < this.currentSize; i++) {
			
			if(temp.getElement().equals(e)) return i;
				
			
			temp = temp.getNext();
		}
		
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		// TODO Auto-generated method stub
		if(this.isEmpty()) {
			return -1;
		}
		
		//if its not empty, we go through the list (but backwards) checking each element, if we find it, we return the index of the object
		Node<E> temp = this.header.getPrevious();
		for (int i = this.currentSize-1; i >= 0 ; i--) {
			
			if(temp.getElement().equals(e)) return i;
				
			
			temp = temp.getPrevious();
		}
		
		return -1;
	}
	
	public Object[] toArray()
	{
		//tried to make things more manageable in car manager class so here we use a for loop to turn the target list into an array 
		//and put every element of the list
		//in that array
		Object[] carArray = new Object[this.size()];
		
		for (int i = 0; i < carArray.length; i++) {
			carArray[i] = (E) this.get(i);
		}
		return carArray;
		
	}
	
}
