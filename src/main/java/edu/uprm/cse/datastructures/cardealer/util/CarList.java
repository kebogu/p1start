package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CarList{
	//we use this class to have some necessary instance variables
	private static CarList singleton = new CarList();
	private static CircularSortedDoublyLinkedList<Car> cList;
	
	private CarList()
	{
		cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		
	}
	//singleton is a new carList instance, so we return it
	public static CarList getInstance()
	{
		return singleton;
		
	}
//we get it by returning it
	public CircularSortedDoublyLinkedList<Car> getList(){
		return  cList;
	}
	//we reset by making a new empty one
	public static void resetCars() {
		cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		
	}
}
